# Wireframes

### Principal
En la página principal tendremos un carrousel que ira mostrando las últimas novedades y mas abajo diferentes secciones como articulos mas buscados, ofertas, etc.  
**Autor: Alejandro**

### Login
La página para que un usuario con sus datos pueda acceder a su cuenta  
**Autor: Alejandro**

### Registro
La página para que un usuario se de de alta en la aplicación  
**Autor: Alejandro**

### Desplegable Categoria
Un desplegable para poder acceder a todos las categorias desde la página principal  
**Autor: Alejandro**

### Desplegable Carrito
Un desplegable para poder ver los articulos que tenemos añadidos al carrito y tambien poder retirarlos del mismo. Tambien tiene un boton para ir a a la página del carrito  
**Autor: Pol**

### Perfil
En la página del perfil el usuario podra consultar la información de su cuenta   
**Autor: Alejandro**

### Vendedor
La página de vendedor es unicamente para los usuarios que tengan permisos para poder introducir productos en la aplicación como por ejemplo empresas  
**Autor: Alejandro**

### Configuracion
La pantalla de configuración servirá para que el usuario pueda realizar cambios en la información de su cuenta  
**Autor: Alejandro**

### Contacto
La página de contacto mostrará la informacion para poder contactar con nosotros en caso de que suceda algún problema o haya alguna duda respecto a la página  
**Autor: Pol**

### Categoria
Una vez seleccionada la categoria esta pantalla mostrará articulos relacionados y podremos usar filtros para concretar más lo que busca el usuario  
**Autor: Pol**

### Producto
La página producto mostrará toda la información referente al producto como las fotos, el precio, etc.  
**Autor: Pol**

### Carrito
En la página carrito podremos ver los productos que tenemos añadidos y varias funciones como seleccionar la cantidad, borrar productos, etc.  
**Autor: Pol**

### Confirmacion
Esta página servirá para que el usuario pueda comprobar que los datos son correctos para poder realizar la compra  
**Autor: Pol**

### Recibo
En esta página se podrá ver los datos de la compra que ha realizado el usuario  
**Autor: Pol**
