## Informe Plantilla Bootstrap

Hem escollit dues plantilles per al nostre projecte, la primera d'on agafariem totes les plantilles necessaries per al nostre projecte, i la segona per agafar els bootstraps de login i registre que no hi havien a la primera plantilla

### Plantilles

[Link Plantilla1](https://themewagon.com/themes/free-responsive-ecommerce-shop-bootstrap-template-supershop/)  
Aquesta plantilla l'hem agafat perque es la que mes s'adapta al tipus de pagina que volem fer i te varies funcionalitats interesants

> - El disseny de la pagina principal s'ajusta a les intencions que teniem des d'un principi i sera fàcil adaptarlo al projecte
- La idea dels desplegables es una mica diferent a la que nosaltres haviem ideat pero els podem aprofitar per millorar aspectes
- Te la possibilitat de posar la capçalera fixa adalt de la página com nosaltres voliem per a que quan fem scroll poguem veure-la sempre
- La gran majoria de les pagines tenen molts aspectes en comú amb les idees que vam plasmar nosaltres als wireframes
- Te diferents dissenys de capçaleras i de peus de pagina per a poder escollir
- Les plantilles dels productes i de les categories tenen tots els elements que necesitem a priori i altres que ens poden vindre molt bé
- Les plantilles de la cistella, confirmacio i rebut son molt semblants a les que nosaltres vam fer als wireframes
- La plantilla de contacte ens ajudara per resoldre alguns dubtes que teniem amb com organitzar tota la informació

D'aquesta primera plantilla agafarem els seguents bootstraps:
- Principal
- Desplegable Carrito
- Desplegable Categoria
- Perfil
- Vendedor
- Configuracio
- Contacte
- Categoria
- Producte
- Carrito
- Confirmacio
- Rebut

[Link Plantilla2](https://themewagon.com/themes/free-responsive-bootstrap-4-html5-hosting-website-template-ecohosting/)  

Aquesta plantilla l'hem agafat perque a la primera plantilla no hi havien els bootstraps de login i registre, i en aquesta ens van agradar ja que son semblants als que volem fer

> - Tant el login com el registre encaixen perfecte amb el que buscabem i haurem de adaptarlos per a que concordin amb els bootstrapsde la primera plantilla

D'aquesta segona plantilla agafarem els seguents bootstraps:
- Login
- Registre

### Bootstraps

Bootstraps que encaixen amb els wireframes
- Principal
- Desplegable Carrito
- Desplegable Categoria
- Contacte
- Categoria
- Producte
- Carrito
- Confirmacio
- Rebut

Bootstraps que necessiten fer adaptacions
- Perfil
- Vendedor
- Configuracio

### HTML amb Bootstrap

Hem comprovat el codi HTML i a primera vista sembla que ho podem adaptar facilment a la nostra idea de projecte i ampliarlo per que es sembli al que vam dissenyar als wireframes
