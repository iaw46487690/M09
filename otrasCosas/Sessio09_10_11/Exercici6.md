# SVG vs CANVAS

**SVG**
- Basado en el modelo de objetos
- Múltiples elementos gráficos que pasan a formar parte del DOM
-La representación visual se genera a partir del markup y se modifica mediante CSS o por programa, mediante scripting

**CANVAS**
-  Orientado al pixel
- Elemento HTML individual, similar en su comportamiento a la etiqueta <img>
-  La representación visual se crea y modifica por programa, mediante scripting
