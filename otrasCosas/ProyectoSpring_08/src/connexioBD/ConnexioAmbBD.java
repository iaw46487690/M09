package connexioBD;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ConnexioAmbBD
 */
@WebServlet("/ConnexioAmbBD")
public class ConnexioAmbBD extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConnexioAmbBD() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String servidorBDUrl = "jdbc:mysql://localhost:3306/LeonovBD_05_a_08?useSSL=false&serverTimezone=UTC";
		String usuari = "root";
		String contrasenya = "";
		String driver = "com.mysql.cj.jdbc.Driver";
		try {
			PrintWriter sortida = response.getWriter();
			sortida.println("INICIANT CONNEXIO AMB LA BD: " + servidorBDUrl);
			Class.forName(driver);
			Connection conexio = DriverManager.getConnection(servidorBDUrl, usuari, contrasenya);
			sortida.println("CONNEXIO AMB EXIT");
			conexio.close();
			sortida.println("CONNEXIO TANCADA AMB EXIT");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
