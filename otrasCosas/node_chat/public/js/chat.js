$(document).ready(function () {
  const socket = io();
  $('#selectRoom').on("change",()=>{
    var sala = $(this).find("option:selected").val();
    socket.emit("changeRoom", $('#selectRoom').val());
    window.history.pushState(null,"Chat","/chat/"+sala);
    $(this).val(sala);
  })

  $("#chat").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    var autor = $("#autor").val();
    $("#chatBox").append(`<p>${autor}: ${msg}<p>`);

    var enviar = {user:autor, text:msg};
    socket.emit("newMsg", enviar);
  });

  socket.on("newMsg", (data)=> {
    $("#chatBox").append(`<p>${data.user}: ${data.text}<p>`);
  })
  socket.on("changeRoom", (sala) => {     
    $("#h1").html('').append(`<p>Chat ${sala}<p>`);
  })
});
