var mongoose = require("mongoose"),
Incidencia = require("../models/incidencias");

// addIncidencia
exports.addIncidencia = async (req, res, next) => {
    const incidencia = new Incidencia({
        nombre: req.body.nombre,
        dni: req.body.dni,
        tipo: req.body.tipo,
        urgencia: req.body.urgencia,
        descripcion: req.body.descripcion
    })
    const add = await incidencia.save();
    if(req.isApi) {
        res.status(201).json(add);
    } else {
        return add;
    }
}

// list
exports.list = async (req, res, next) => {
    const incidencia = await Incidencia.find();
    if (req.isApi) {
        res.status(200).json(incidencia);
    } else {
        return incidencia;
    }
}

// listIncidencia
exports.listIncidencia = async (req, res, next) => {
    const incidencia = await Incidencia.findById(req.params.id);
    if(req.isApi) {
        res.status(200).json(incidencia);
    } else {
        return incidencia;
    }    
}

// deleteIncidencia
exports.deleteIncidencia = async (req, res) => {
    const incidencia = await Incidencia.findById(req.params.id);
    const resultado = incidencia.delete();
    if (req.isApi) {
        res.json({ mesagge: "Se ha borrado correctamente"});
    } else {
        return "Se ha borrado correctamente";
    } 
}

// updateIncidencia
exports.updateIncidencia = async (req,res) => {
    const incidencia = {
        nombre: req.body.nombre,
        dni: req.body.dni,
        tipo: req.body.tipo,
        urgencia: req.body.urgencia,
        descripcion: req.body.descripcion
    }
    const update = await Incidencia.updateOne({_id: req.params.id}, incidencia)
    if(req.isApi) {
        res.status(200).json({ mesagge: "Se ha actualizado correctamente"});
    } else {
        return "Se ha actualizado correctamente";
    }
}

// listIncidenciaByName
exports.listIncidenciaByName = async (req, res, next) => {
    const incidencia = await Incidencia.find({}, { projection: { nombre: req.params.nombre } });
    if(req.isApi) {
        res.status(200).json(incidencia);
    } else {
        return incidencia;
    }    
}