var express = require("express");
var router = express.Router();
var path = require("path");
//Controllers
var ctrlDir = "/app/app/controllers";
var incidenciaCtrl = require(path.join(ctrlDir, "incidencias"));

// Incidencias
router.get("/incidencia", incidenciaCtrl.list);
router.get("/incidencia/:id", incidenciaCtrl.listIncidencia);
router.post("/incidencia", incidenciaCtrl.addIncidencia);
router.delete("/incidencia/:id", incidenciaCtrl.deleteIncidencia);
router.put("/incidencia/:id", incidenciaCtrl.updateIncidencia);
router.get("/incidencia/:nombre", incidenciaCtrl.listIncidenciaByName);

module.exports = router;
