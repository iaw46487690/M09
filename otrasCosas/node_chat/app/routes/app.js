var express = require("express");
var router = express.Router();
var path = require("path");
var ctrlDir = "/app/app/controllers";
var incidenciaCtrl = require(path.join(ctrlDir, "incidencias"));


router.get("/incidencias", function(req, res, next) {
    res.render("incidencias")
});

router.post("/enviarFormulari", async function(req, res, next) {
    result = await incidenciaCtrl.addIncidencia(req);
    res.send("Incidencia añadida correctamente");
});

router.get("/listaIncidencias", async function(req, res, next) {
    incidencias = await incidenciaCtrl.list(req);
    res.render("listaIncidencias", {listaIncidencias: incidencias})
});

router.get("/", function(req, res, next) {
    res.render("chat")
});

router.get("/chat", function(req, res, next) {
    res.render("chat")
});

router.get("/chat/:id", function(req, res, next) {
    var sala = req.params.id;
    console.log(sala);
    res.render("chat")
});

module.exports = router;

