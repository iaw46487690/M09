<?php $__env->startSection('content'); ?>
<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card">
      <div class="card-header"><?php echo e(__('Register')); ?></div>

      <div class="card-body">
        <form method="POST" action="/compra/envio" enctype="multipart/form-data">
          <?php echo csrf_field(); ?>

          <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right"><?php echo e(('Name')); ?></label>
            <div class="col-md-6">
              <input id="name" type="text" class="form-control" name="name" value="<?php echo e($name); ?>" autocomplete="name" autofocus>
            </div>
          </div>

          <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right"><?php echo e(('E-Mail Address')); ?></label>

            <div class="col-md-6">
              <input id="email" type="email" class="form-control" name="email" value="<?php echo e($email); ?>" autocomplete="email">
            </div>
          </div>

          <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right"><?php echo e(('Direccion')); ?></label>

            <div class="col-md-6">
              <input id="email" type="text" class="form-control" name="address" value="<?php echo e(old('address')); ?>">
            </div>
          </div>

          <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right"><?php echo e(('Password')); ?></label>

            <div class="col-md-6">
              <input id="password" type="password" class="form-control" name="password">
            </div>
          </div>

          <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right"><?php echo e(('Confirm Password')); ?></label>

            <div class="col-md-6">
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
            </div>
          </div>

          <div class="form-group row">
            <label for="foto" class="col-md-4 col-form-label text-md-right"><?php echo e(('Foto')); ?></label>

            <div class="col-md-6">
              <input id="photo" type="file" class="form-control" name="photo">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-6">
              <input id="name" type="hidden">
              <span role="alert">
                <!--TODO: Mensajes de errores de formulario-->
                <?php if($errors->any()): ?>
                    <div class="alert" style="color:#c20000;">
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($error); ?> <br>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <br>
                    </div>
                <?php endif; ?>
                <!--Fin del Bloque-->
              </span>
            </div>
          </div>
          <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                <?php echo e(__('Pagar')); ?>

              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<a href="<?php echo e(url('/compra/resumen')); ?>" class="btn btn-secondary btn-lg float-left">Atras</a>
<!--<a href="<?php echo e(url('/compra/confirmar')); ?>"  class="btn btn-primary btn-lg float-right">Siguiente</a>-->

<br><br>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /app/resources/views/compra/envio.blade.php ENDPATH**/ ?>