<?php $__env->startSection('content'); ?>
<div class="row">

  <div class="col-lg-3">
    <!--TODO: SEARCH-->
    <h2 class="my-4">Buscar <br>(en este genero)</h2>
    <form method="POST" action="/search">
      <?php echo e(csrf_field()); ?>

      <input type="hidden" name="category" value="<?php echo e(Request()->id ?? old('category')); ?>">
      <div class="form-group">
        <div class="form-check">
          <input name="search" class="form-text" type="text">
        </div>
      </div>
    </form>
    <!--Fin BLOQUE-->


    <!--TODO: SELECCIONAR GENERO-->
    <h2 class="my-4">Genero</h2>
        <div class="list-group">
            <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filter): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $__currentLoopData = $filter; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(url('category', $key)); ?>" class="list-group-item"><?php echo e($category); ?></a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    <!--Fin BLOQUE-->

  </div>

  <div class="col-lg-9">
    <!--TODO: BANNER-->
    <?php if($showBanner): ?>
    <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
          <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <?php endif; ?>
    <!--Fin BLOQUE-->


    <!--TODO: LISTA DE PELICULAS-->
    <div class="row">
        <?php $__empty_1 = true; $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="card h-100">
                    <a href="#"><img class="card-img-top" src="<?php echo e(asset('img/') . '/' . $prod->image); ?>" alt=""></a>
                <div class="card-body">
                    <h4 class="card-title">
                    <a href="#"><?php echo e($prod->name); ?></a>
                    </h4>
                    <h5>$<?php echo e($prod->price); ?></h5>
                    <p class="card-text"><?php echo e($prod->descripcion); ?></p>
                </div>
                <div class="card-footer">
                    <form class="addCart" action="/addToCart" method="post">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="id" value="<?php echo e($prod->id); ?>">
                        <input type="hidden" name="name" value="<?php echo e($prod->name); ?>">
                        <input type="hidden" name="category" value="<?php echo e($prod->category); ?>">
                        <input type="hidden" name="descripcion" value="<?php echo e($prod->descripcion); ?>">
                        <input type="hidden" name="rating" value="<?php echo e($prod->rating); ?>">
                        <input type="hidden" name="stock" value="<?php echo e($prod->stock); ?>">
                        <input type="hidden" name="price" value="<?php echo e($prod->price); ?>">
                        <input type="hidden" name="image" value="<?php echo e($prod->image); ?>">
                        <input type="submit" class="btn-block btn-primary" name="action" value="Comprar">
                        <input type="submit" class="btn-block btn-primary" name="action" value="Alquilar">
                    </form>
                </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <!--SI no hay productos mostrar:-->
            <p>No products to show</p>
        <?php endif; ?>
    </div>

    <!--Fin BLOQUE-->


  </div>

</div>

<!--TODO: ACCIONES DE CARRITO-->
<script>
  //Escribir aqui el codigo necesario de AXIOS
  /*$('.addCart').submit(function(e) {
    e.preventDefault();
    var data = $(this).serialize();
    console.log(data);
    // Guardamos la cantidad de productos a partir de la string data
    var cantidad = parseInt(data.charAt(data.length - 1));
    // Peticion para comprobar que hay stock
    axios.post('/stock', data)
      .then(response => {
        // Si se ha encontrado stock (1), cambiamos el valor del carrito i añadimos al carrito
        if (response.data == 1) {
          // Guardamos la cantidad actual del carrito
          axios.post('/addToCart', data)
            .then(response => {
              // Cambiamos el valor del carrito
              console.log("cantidad carrito :" + response.data);
              $('#carrito').html(Number(response.data) + cantidad);
            })
          // Añadimos al carrito
          axios.post('/addToCart', data)
            .then(response => {
              //console.log(response.data);
            })
        } else {
          alert("Lo sentimos, no tenemos " + cantidad + " unidades de este producto actualmente");
        }
      })
    //Sustituir 0 por la cantidad que tengamos en el carrito
    //$('#carrito').html(0);
  })*/

  //Al hacer click se obtiene la info del producto para saber si hay stock
  //Si hay stock se envia una petición para guardar en el carrito ese producto. SE guardara como comprado o alquilado
</script>
 <!--Fin BLOQUE-->

<?php $__env->stopSection(); ?>

<?php echo $__env->make((Request::ajax()) ? 'layouts.ajax' : 'layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /app/resources/views/productos/products.blade.php ENDPATH**/ ?>