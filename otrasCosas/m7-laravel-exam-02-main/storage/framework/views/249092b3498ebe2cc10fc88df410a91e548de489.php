<?php $__env->startSection('content'); ?>
<?php ($total=0); ?>
<?php ($numProducts=0); ?>
<!--TODO: Calculo de precios-->
<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if($product->action == "rent"): ?>
        <?php ($total+=5); ?>
    <?php else: ?>
        <?php ($total+=$product->price); ?>
    <?php endif; ?>
    <?php ($numProducts++); ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<!--Fin del Bloque-->
<div class="row">

  <div class="col-lg-3">

    <h1 class="my-4">Lego Shop</h1>
    <ul class="list-group">
      <li class="list-group-item">
        Total de productos: <?php echo e($numProducts); ?></li>
      <li class="list-group-item">
        Precio total de productos: $<?php echo e($total); ?></li>
    </ul>



  </div>

  <div class="col-lg-9">
    <div class="row">
      <?php $__empty_1 = true; $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <a href="#"><img class="card-img-top" src="<?php echo e(asset('img/'.$product->image)); ?>" alt=""></a>
          <div class="card-body">
            <h3 class="card-title">
              <a href="#"><?php echo e($product->name); ?></a>
            </h3>
            <h4>to <?php echo e($product->action); ?></h4>
            <h5>$<?php echo e($product->price); ?></h5>
          </div>
        </div>
      </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
      <p>No products to show</p>
      <?php endif; ?>
    </div>
  </div>
</div>


<a href="<?php echo e(url('/')); ?>" class="btn btn-secondary btn-lg float-left">Atras</a>
<a href="<?php echo e(url('/compra/envio')); ?>" class="btn btn-primary btn-lg float-right">Siguiente</a>
<br><br>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /app/resources/views/compra/resumen.blade.php ENDPATH**/ ?>