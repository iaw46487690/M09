<?php $__env->startSection('content'); ?>
<?php ($total=0); ?>
<?php ($numProducts=0); ?>
<!--TODO: Calculo de precios-->
<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if($product->action == "rent"): ?>
        <?php ($total+=5); ?>
    <?php else: ?>
        <?php ($total+=$product->price); ?>
    <?php endif; ?>
    <?php ($numProducts++); ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<!--Fin del Bloque-->
<div class="row">

  <div class="col-lg-3">

    <h1 class="my-4">Lego Shop</h1>
    <ul class="list-group">
      <li class="list-group-item">
        Total de productos: <?php echo e($numProducts); ?></li>
      <li class="list-group-item">
        Precio total de productos: <?php echo e($total); ?></li>
    </ul>
  </div>

  <div class="col-lg-9">
    <div class="card">
      <div class="card-header"><?php echo e(__('Datos de envio')); ?></div>
      <div class="card-body">
        <ul>
          <?php $__currentLoopData = $shipping; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($key); ?> <?php echo e($info); ?></li>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </ul>
      </div>
    </div>

  </div>
</div>

<a href="<?php echo e(url('/compra/envio')); ?>" class="btn btn-secondary btn-lg float-left">Atras</a>
<a href="#" class="btn btn-primary btn-lg float-right">Comprar</a>
<br><br>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /app/resources/views/compra/confirmar.blade.php ENDPATH**/ ?>