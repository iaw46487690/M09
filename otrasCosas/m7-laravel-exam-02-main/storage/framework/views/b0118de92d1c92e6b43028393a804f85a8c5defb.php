<?php $__env->startSection('content'); ?>
<div class="row">
  <div class="col-lg-12" style="height: 740px">
    <br>
    <h2 class="text-center">Todos los productos han sido eliminados</h2>
    <br>
    <a href="<?php echo e(url('/')); ?>" class="btn btn-success">Volver a Inicio</a>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /app/resources/views/clearCart.blade.php ENDPATH**/ ?>