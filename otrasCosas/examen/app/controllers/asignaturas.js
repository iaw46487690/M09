//var mongoose = require("mongoose"),
Asignatura = require("../models/asignatura");
// load from DB function
exports.save = async (req, res, next) => {
    const asignatura = new Asignatura({
        nombre: req.body.name,
        numHoras: req.body.numHoras,
        docente: "http://127.0.0.1:3000/api/docentes/" + req.body.docente,
        alumno: req.body.alumno
    })
    try {
        const guardado = await asignatura.save();
        if (req.isApi) {
            res.status(201).json(guardado);
        } else {
            return guardado;
        }
    } catch (error) {
        console.log(error);
    }
}
