var express = require("express");
var router = express.Router();
var path = require("path");
//Controllers
var ctrlDir = "/app/app/controllers";
var alumnrCtrl = require(path.join(ctrlDir, "alumnos"));
var docenteCtrl = require(path.join(ctrlDir, "docentes"));


// Alumnos
router.get("/alumnos", alumnrCtrl.load);
router.get("/alumnos/:id", alumnrCtrl.loadOne);
router.post("/alumnos", alumnrCtrl.add);
router.delete("/alumnos/:id", alumnrCtrl.delete);
router.put("/alumnos/:id", alumnrCtrl.update);

// Docentes
router.get("/docentes", docenteCtrl.load);
router.get("/docentes/:id", docenteCtrl.loadOne);
router.post("/docentes", docenteCtrl.add);
router.delete("/docentes/:id", docenteCtrl.delete);
router.put("/docentes/:id", docenteCtrl.update);

module.exports = router;
